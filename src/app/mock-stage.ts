import { Stage} from "./stage";

export const STAGES: Stage[]=[{

    id:1,
    titre:"Stage en développement web",
    description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam congue eros vitae magna tincidunt dictum. Sed at justo ac leo tristique tincidunt. Curabitur cursus velit eget dolor gravida placerat. Aenean eu est hendrerit, porttitor lacus id, tempor sem. Duis porttitor luctus efficitur. Etiam nec ante condimentum, euismod erat quis, bibendum urna.",

},{
    id:2,
    titre:"Stage Product Owner",
    description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in metus augue. Maecenas turpis magna, commodo non semper at, feugiat ut nunc. Duis pellentesque fermentum lectus non hendrerit. Mauris auctor eget nulla a venenatis. Nam lacinia est elit, vel tempus nunc rhoncus ut. ",
},
{
    id:3,
    titre:"Stage data analyst",
    description:"Ut non fringilla enim. Curabitur in orci massa. Cras fermentum diam id sodales fermentum. Nullam sed efficitur justo, consectetur tincidunt libero. Proin iaculis tellus velit, sit amet aliquet justo tempor laoreet. Curabitur elit tellus, interdum et libero id, ultrices lobortis odio. Integer id gravida orci.",
},{
    id:4,
    titre:"Stage en tansition écologique",
    description:"Cras varius mattis risus, vitae viverra nisi maximus et. Maecenas pellentesque vehicula mauris et tempus. Quisque et gravida turpis, ut tristique libero. Donec eu sapien id tellus dictum lobortis at sed erat. Vestibulum rhoncus venenatis aliquam. Phasellus auctor quis sem vel auctor. Cras ut condimentum sapien, ut tincidunt turpis. ",
},{
    id:5,
    titre:"stage management du risque",
    description:"Ut vitae tellus erat. In hac habitasse platea dictumst. Aenean eu rhoncus mauris, eu pharetra velit. Nulla efficitur a magna non gravida. Cras commodo enim in mauris porttitor ullamcorper. Praesent mauris neque, pulvinar nec ultricies sed, euismod vitae ex. Donec at neque eget tellus sodales volutpat. ",
},{
    id:6,
    titre:"stage consultant en cybersécurité",
    description:"Nullam porttitor purus a orci mollis, sit amet dictum erat accumsan. Fusce eu euismod dolor, a consectetur purus. Duis tempor dui odio. Nam at bibendum sapien. Aliquam aliquam consequat risus in tempus. Donec id egestas nisl. Etiam semper volutpat diam, vitae tristique sem aliquet id. Mauris viverra eleifend odio sed hendrerit.",
},{
    id:7,
    titre:"Stage coordination projet télécom",
    description:"Nulla facilisi. Ut commodo dui at eros rutrum vulputate. Morbi at tellus eu enim dapibus maximus vitae sit amet quam. Cras nec lectus ullamcorper, elementum magna vel, ultricies purus. Vivamus finibus fermentum tortor. Nullam suscipit, orci a lacinia efficitur, elit dui bibendum arcu, ac rutrum ante metus in libero. Nullam elementum eros et est malesuada, ac consectetur nunc sodales. ",
},{
    id:8,
    titre:"stage consultant transformation digitale",
    description:"Vivamus congue ipsum ante, ac pellentesque nunc cursus eu. In aliquam turpis erat, fringilla faucibus nunc dictum nec. Cras feugiat mattis nibh, eget dignissim nulla ornare nec. Aliquam malesuada, mi sit amet tincidunt condimentum, nulla ipsum posuere orci, ut blandit lacus dui sit amet magna. Integer pretium nisi eget scelerisque blandit. In dapibus arcu justo, vel convallis tortor viverra sed. Suspendisse nec egestas mauris.",
}];