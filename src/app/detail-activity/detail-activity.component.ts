import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Activity } from '../activity';
import { ActivityServiceService } from '../activity-service.service';

@Component({
  selector: 'app-detail-activity',
  templateUrl: './detail-activity.component.html',
  styleUrls: ['./detail-activity.component.css']
})
export class DetailActivityComponent {
  activity! :Activity;
  constructor(private activityservice: ActivityServiceService, private route: ActivatedRoute, private router: Router){

  }

  ngOnInit():void{

    const activityname = this.route.snapshot.params['name'];
    this.activity=this.activityservice.getname(activityname);
  }
}
