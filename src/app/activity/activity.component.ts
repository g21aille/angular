import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Activity } from '../activity';
import { ActivityServiceService } from '../activity-service.service';


@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})
export class ActivityComponent {

  constructor(private activityservice : ActivityServiceService, private router: Router){}
  @Input() activity!: Activity;
  select(){
      console.log(this.activity.name);
      this.router.navigateByUrl(`activity/${this.activity.name}`)
    }
}
