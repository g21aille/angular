import { Component } from '@angular/core';
import { Stage } from '../stage';
import { StageServiceService } from '../stage-service.service';

@Component({
  selector: 'app-stage-accordion',
  templateUrl: './stage-accordion.component.html',
  styleUrls: ['./stage-accordion.component.css']
})
export class StageAccordionComponent {
    stages: Stage[]=[]

    constructor(private StageService :StageServiceService){
      this.stages=this.StageService.get();
    }
}
