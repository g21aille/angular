import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StageAccordionComponent } from './stage-accordion.component';

describe('StageAccordionComponent', () => {
  let component: StageAccordionComponent;
  let fixture: ComponentFixture<StageAccordionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StageAccordionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StageAccordionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
