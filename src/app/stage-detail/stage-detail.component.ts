import { Component, Input } from '@angular/core';
import { Stage } from '../stage';
import { StageServiceService } from '../stage-service.service';

@Component({
  selector: 'app-stage-detail',
  templateUrl: './stage-detail.component.html',
  styleUrls: ['./stage-detail.component.css']
})
export class StageDetailComponent {
    @Input() stage! : Stage;

    constructor(private StageService: StageServiceService){}
}
