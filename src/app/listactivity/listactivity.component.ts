import { Component, Input, ChangeDetectorRef } from '@angular/core';
import { Activity } from '../activity';
import { ActivatedRoute, Router } from '@angular/router';
import { ActivityServiceService } from '../activity-service.service';

@Component({
  selector: 'app-listactivity',
  templateUrl: './listactivity.component.html',
  styleUrls: ['./listactivity.component.css']
})
export class ListactivityComponent {
  act!: Activity[];
  type!: string;
  constructor(private activityservice : ActivityServiceService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit():void{
    const listActivityType = this.route.snapshot.params['type'];
    this.type = listActivityType
    this.act = this.activityservice.getByType(this.type);
  }


}
