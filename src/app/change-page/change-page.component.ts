import { Component } from '@angular/core';

@Component({
  selector: 'app-change-page',
  templateUrl: './change-page.component.html',
  styleUrls: ['./change-page.component.css']
})

export class ChangePageComponent {
  changeclass(): void {
    var b1 = document.getElementById("border")  
    var accordion = document.getElementById("accordion")  
    var stage = document.getElementById("stage")  
    console.log(stage)
    console.log(accordion)
    if(b1 != null)
    {
      var b2 = document.getElementById("border2")
      if(b1.matches(":hover")) {
        if(b2 != null){
          if(b1.className == "border"){
            if(accordion != null){
              if(stage){
                accordion.className = "show"
                stage.className = "hide"
              }
              
            }
            b1.className="bordertest"
            b2.className="bordertest2"}
          }
      }
      if(b2 != null){
        if(b2.matches(":hover")) {     
          if(b1.className == "bordertest"){
            if(accordion != null){
              if(stage){
                accordion.className = "hide"
                stage.className = "show"
              }
              
            }
            b1.className="border"
            b2.className="border2"}
          }
      }
    } 
}
}