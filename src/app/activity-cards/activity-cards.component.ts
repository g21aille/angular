import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ListactivityComponent } from '../listactivity/listactivity.component';
@Component({
  selector: 'app-activity-cards',
  templateUrl: './activity-cards.component.html',
  styleUrls: ['./activity-cards.component.css']
})
export class ActivityCardsComponent {
  @ViewChild('listactivity') listactivity!: ListactivityComponent;
  constructor(private router: Router){}
  changeType(type:string): void {
    this.router.navigateByUrl(`activities/${type}`)
  }

  }

