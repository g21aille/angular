export interface Activity {
    type:string
    name:string
    date:Date
    description:string
    description_detail:string
}
