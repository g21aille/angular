import { Injectable } from '@angular/core';
import { Activity } from './activity';
import { ACTIVITIES } from './mock-activity';

@Injectable({
  providedIn: 'root'
})
export class ActivityServiceService {
  listactivity! : Activity[];

  constructor() { }
  get(){
    return this.listactivity=ACTIVITIES;
  }

  getByType(type:string){
    return this.listactivity = ACTIVITIES.filter(activity => activity.type === type)
  }

  getname(activityname : string) {
      const activity = ACTIVITIES.find(activity => activity.name === activityname);
            if (!activity) {
                throw new Error('name not found!');
            } else {
                return activity;
            }
  }
}
