import { Injectable } from '@angular/core';
import { Stage } from './stage';
import { STAGES } from './mock-stage';

@Injectable({
  providedIn: 'root'
})
export class StageServiceService {

  constructor() { }

  get(): Stage[]{
      return(STAGES);
  }
}
