import { Activity } from "./activity";

export const ACTIVITIES: Activity[] = [
    {
        type:"Sport",
        name:"Football",
        date:new Date(30/10/2060),
        description:"J'adore le foot c'est cool !",
        description_detail:"lgighoiihgfdiofoihfiskdjfksdjfmlskdjflmkjfklsmjdfdslfjsdmlkfjsdlmfjdsklfjdslmfkjdflmsjmfldjflsjfkldfmlsdjkfklsdfjsdlfjslmfdjflsj",
    },
    {
        type:"Sport",
        name:"Curling",
        date:new Date(11/11/1111),
        description:"Est-ce un sport ?",
        description_detail:"curlinglgighoiihgfdiofoihfiskdjfksdjfmlskdjflmkjfklsmjdfdslfjsdmlkfjsdlmfjdsklfjdslmfkjdflmsjmfldjflsjfkldfmlsdjkfklsdfjsdlfjslmfdjflsj",
    },
    {
        type:"Afterwork",
        name:"Bar",
        date:new Date(15/15/2015),
        description:"Objectif 4g",
        description_detail:"barlgighoiihgfdiofoihfiskdjfksdjfmlskdjflmkjfklsmjdfdslfjsdmlkfjsdlmfjdsklfjdslmfkjdflmsjmfldjflsjfkldfmlsdjkfklsdfjsdlfjslmfdjflsj",
    },
    {
        type:"Conférence",
        name:"Avis soutenance de thèse n°404",
        date:new Date(5/5/5005),
        description:"bla bla bla",
        description_detail:"thèse1lgighoiihgfdiofoihfiskdjfksdjfmlskdjflmkjfklsmjdfdslfjsdmlkfjsdlmfjdsklfjdslmfkjdflmsjmfldjflsjfkldfmlsdjkfklsdfjsdlfjslmfdjflsj",
    },
    {
        type:"Conférence",
        name:"Avis soutenance de thèse n°8687",
        date:new Date(14/5/10500),
        description:"bla bla bla",
        description_detail:"thèse2lgighoiihgfdiofoihfiskdjfksdjfmlskdjflmkjfklsmjdfdslfjsdmlkfjsdlmfjdsklfjdslmfkjdflmsjmfldjflsjfkldfmlsdjkfklsdfjsdlfjslmfdjflsj",
    }
];