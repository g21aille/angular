import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActivityComponent } from './activity/activity.component';
import { DetailActivityComponent } from './detail-activity/detail-activity.component';
import { ListactivityComponent } from './listactivity/listactivity.component';
import { ActivityCardsComponent } from './activity-cards/activity-cards.component';
import { ChangePageComponent } from './change-page/change-page.component';


const routes: Routes = [
  {path:'activities/:type', component:ListactivityComponent},
  {path:'activity/:name', component:DetailActivityComponent},
  {path:'', component:ChangePageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
