import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ActivityCardsComponent } from './activity-cards/activity-cards.component';
import { ActivityContextDirective } from './activity-context.directive';
import { ChangePageComponent } from './change-page/change-page.component';
import { ListactivityComponent } from './listactivity/listactivity.component';
import { ActivityComponent } from './activity/activity.component';
import { DetailActivityComponent } from './detail-activity/detail-activity.component';
import { StageAccordionComponent } from './stage-accordion/stage-accordion.component';
import { StageDetailComponent } from './stage-detail/stage-detail.component';


@NgModule({
  declarations: [
    AppComponent,
    ActivityCardsComponent,
    ActivityContextDirective,
    ChangePageComponent,
    ListactivityComponent,
    ActivityComponent,
    DetailActivityComponent,
    StageAccordionComponent,
    StageDetailComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
